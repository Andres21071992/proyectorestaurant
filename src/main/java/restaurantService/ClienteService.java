package restaurantService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import restaurant.Bussines.ClienteBussines;

import restaurant.Models.Cliente;
import restaurant.Models.Pedido;
import restaurant.Models.PedidoAux;


@Path("/personas")
public class ClienteService {
	
	@Inject
	private ClienteBussines cb;
	
	@GET
	@Path("/buscar")
	@Produces("application/json")
	public Cliente buscarCliente(@QueryParam("cedula")String cedula)
	{
		return cb.buscarCliente(cedula);
	}
	
	@GET
	@Path("/login")
	@Produces("application/json")
	public Cliente  iniciarSercion (@QueryParam("usuario") String usuario,@QueryParam("clave") String clave){
		return cb.iniciarSesion(usuario, clave);
	}
	@GET
	@Path("/listapedidos")
	@Produces("application/json")
	public List<Pedido> obtenerPEdidos (@QueryParam("cedula") String cedula){
		return cb.obtenerPedidos(cedula);
	}
	@POST
	@Path("/agregar")
	@Produces("application/json")
	public Response addCliente(Cliente cliente)
	{
		Response.ResponseBuilder builder = null;
		Map<String,String> data = new HashMap();
		try
		{
			cb.insertarCliente(cliente);
			data.put("code", "1");
			data.put("message", "OK");
			builder = Response.status(Response.Status.OK).entity(data);
		}catch(Exception e)
		{
			data.put("code", "99");
			data.put("message", e.getMessage());
			builder = Response.status(Response.Status.OK).entity(data);
		}
		return builder.build();
	}
	@POST
	@Path("/editar")
	@Produces("application/json")
	public Response modifyCliente(Cliente cliente)
	{
		Response.ResponseBuilder builder = null;
		Map<String,String> data = new HashMap();

		try
		{
			cb.actualizarCliente(cliente);
			data.put("code", "1");
			data.put("message", "OK");
			builder = Response.status(Response.Status.OK).entity(data);
		}catch(Exception e)
		{
			data.put("code", "99");
			data.put("message", e.getMessage());
			builder = Response.status(Response.Status.OK).entity(data);
		}
		return builder.build();
	}
	@GET
	@Path("/eliminar")
	@Produces("application/json")
	public Response eliminarCliente(@QueryParam("cedula")String cedula)
	{
		Response.ResponseBuilder builder = null;
		Map<String,String> data = new HashMap();
		try
		{
			cb.eliminarCliente(cedula);;
			data.put("code", "1");
			data.put("message", "OK");
			builder = Response.status(Response.Status.OK).entity(data);
		}catch(Exception e)
		{
			data.put("code", "99");
			data.put("message", e.getMessage());
			builder = Response.status(Response.Status.OK).entity(data);
		}
		return builder.build();
	}
	@POST
	@Path("/agregarpedido")
	@Produces("application/json")
	public String addPedido(PedidoAux pedido)
	{
		String resp="correcto";
		try
		{
			cb.agregarPedido(pedido);
		}catch(Exception e)
		{
			resp="incorrecto";
		}
		return resp;
	}
	
}
