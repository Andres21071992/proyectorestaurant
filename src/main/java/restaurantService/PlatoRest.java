package restaurantService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import restaurant.Bussines.PedidoBussines;
import restaurant.Bussines.PlatoBussines;
import restaurant.Models.Ingrediente;
import restaurant.Models.Pedido;
import restaurant.Models.Platos;

@Path("/platos")
public class PlatoRest {
	@Inject
	private PlatoBussines pBussines;
	@GET
	@Path("/listado")
	@Produces("application/json")
	public List<Platos> obtenerPlatos(){
		return pBussines.obtenerPlatos();
	}
	@GET
	@Path("/filtrarplatos")
	@Produces("application/json")
	public List<Platos> obtenerPlatosxnombre(@QueryParam("nombre")String nombre){
		return pBussines.obtenerPlatosnombre(nombre);
	}
	@GET
	@Path("/ingrediente")
	@Produces("application/json")
	public List<Ingrediente> obtenerIngrediente(@QueryParam("plato")String codigo){
		return pBussines.getIngrediente(codigo);
	}
	@POST
	@Path("/insertar")
	@Produces("application/json")
	@Consumes("application/json")
	public Response insertar (Platos p) {
		
		Response.ResponseBuilder builder = null;
		Map<String, String> data = new HashMap<>();
		
		try {
			
			pBussines.insertarPlato(p);
			data.put("code", "1");
			data.put("message", "OK");
			builder = Response.status(Response.Status.OK).entity(data);
			
			
		}catch (Exception e) {
			e.printStackTrace();
			data.put("code", "99");
			data.put("message", e.getMessage());
			builder = Response.status(Response.Status.OK).entity(data);
			
		}	
		return builder.build();
	}
	@POST
	@Path("/eliminar")
	@Produces("application/json")
	@Consumes("application/json")
	public Response eliminar (String cod) {	
		Response.ResponseBuilder builder = null;
		Map<String, String> data = new HashMap<>();
		
		try {
			
			pBussines.eliminarPlato(cod);
			data.put("code", "1");
			data.put("message", "OK");
			builder = Response.status(Response.Status.OK).entity(data);
			
			
		}catch (Exception e) {
			e.printStackTrace();
			data.put("code", "99");
			data.put("message", e.getMessage());
			builder = Response.status(Response.Status.OK).entity(data);
			
		}	
		return builder.build();
	}
	@POST
	@Path("/modificar")
	@Produces("application/json")
	@Consumes("application/json")
	public Response modificar (Platos p) {
		
		Response.ResponseBuilder builder = null;
		Map<String, String> data = new HashMap<>();
		
		try {
			
			pBussines.actualizarPlato(p);
			data.put("code", "1");
			data.put("message", "OK");
			builder = Response.status(Response.Status.OK).entity(data);
			
		}catch (Exception e) {
			e.printStackTrace();
			data.put("code", "99");
			data.put("message", e.getMessage());
			builder = Response.status(Response.Status.OK).entity(data);
			
		}	
		return builder.build();
	}

}
