package restaurantService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import restaurant.Bussines.BebidaBussines;
import restaurant.Bussines.PedidoBussines;
import restaurant.Models.Bebida;
import restaurant.Models.DetallePedido;
import restaurant.Models.Pedido;

@Path("/pedidos")
public class PedidoRest {
	@Inject
	private PedidoBussines pBussines;
	@GET
	@Path("/listado")
	@Produces("application/json")
	public List<Pedido> obtenerPedidos(){
		return pBussines.obtenerPedido();
	}
	@GET
	@Path("/listadodetalle")
	@Produces("application/json")
	public List<DetallePedido> obtenerDetalles(@QueryParam("codigo")String pedido){
		return  pBussines.obtenerDetalles(pedido);		
	}
	@POST
	@Path("/insertar")
	@Produces("application/json")
	@Consumes("application/json")
	public Response insertar (Pedido p) {
		
		Response.ResponseBuilder builder = null;
		Map<String, String> data = new HashMap<>();
		
		try {
			
			pBussines.insertarPedido(p);
			data.put("code", "1");
			data.put("message", "OK");
			builder = Response.status(Response.Status.OK).entity(data);
			
			
		}catch (Exception e) {
			e.printStackTrace();
			data.put("code", "99");
			data.put("message", e.getMessage());
			builder = Response.status(Response.Status.OK).entity(data);
			
		}	
		return builder.build();
	}
	@POST
	@Path("/eliminar")
	@Produces("application/json")
	@Consumes("application/json")
	public Response eliminar (String cod) {	
		Response.ResponseBuilder builder = null;
		Map<String, String> data = new HashMap<>();
		
		try {
			
			pBussines.eliminarPedido(cod);
			data.put("code", "1");
			data.put("message", "OK");
			builder = Response.status(Response.Status.OK).entity(data);
			
			
		}catch (Exception e) {
			e.printStackTrace();
			data.put("code", "99");
			data.put("message", e.getMessage());
			builder = Response.status(Response.Status.OK).entity(data);
			
		}	
		return builder.build();
	}
	@POST
	@Path("/modificar")
	@Produces("application/json")
	@Consumes("application/json")
	public Response modificar (Pedido p) {
		
		Response.ResponseBuilder builder = null;
		Map<String, String> data = new HashMap<>();
		
		try {
			
			pBussines.actualizarPedido(p);
			data.put("code", "1");
			data.put("message", "OK");
			builder = Response.status(Response.Status.OK).entity(data);
			
		}catch (Exception e) {
			e.printStackTrace();
			data.put("code", "99");
			data.put("message", e.getMessage());
			builder = Response.status(Response.Status.OK).entity(data);
			
		}	
		return builder.build();
	}
}
