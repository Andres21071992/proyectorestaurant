package restaurantService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import restaurant.Bussines.BebidaBussines;

import restaurant.Models.Bebida;


@Path("/bebidas")
public class BebidaRest {
	@Inject
	private BebidaBussines bBussines;
	@GET
	@Path("/listado")
	@Produces("application/json")
	public List<Bebida> obtenerBebida(){
		return bBussines.obtenerBebida();
	}
	
	@POST
	@Path("/insertar")
	@Produces("application/json")
	@Consumes("application/json")
	public Response insertar (Bebida b) {
		
		Response.ResponseBuilder builder = null;
		Map<String, String> data = new HashMap<>();
		
		try {
			
			bBussines.insertarBebida(b);
			data.put("code", "1");
			data.put("message", "OK");
			builder = Response.status(Response.Status.OK).entity(data);
			
			
		}catch (Exception e) {
			e.printStackTrace();
			data.put("code", "99");
			data.put("message", e.getMessage());
			builder = Response.status(Response.Status.OK).entity(data);
			
		}	
		return builder.build();
	}
	
	@POST
	@Path("/eliminar")
	@Produces("application/json")
	@Consumes("application/json")
	public Response eliminar (String cod) {	
		Response.ResponseBuilder builder = null;
		Map<String, String> data = new HashMap<>();
		
		try {
			
			bBussines.eliminarBebida(cod);
			data.put("code", "1");
			data.put("message", "OK");
			builder = Response.status(Response.Status.OK).entity(data);
			
			
		}catch (Exception e) {
			e.printStackTrace();
			data.put("code", "99");
			data.put("message", e.getMessage());
			builder = Response.status(Response.Status.OK).entity(data);
			
		}	
		return builder.build();
	}
	@POST
	@Path("/modificar")
	@Produces("application/json")
	@Consumes("application/json")
	public Response modificar (Bebida b) {
		
		Response.ResponseBuilder builder = null;
		Map<String, String> data = new HashMap<>();
		
		try {
			
			bBussines.actualizarBebida(b);
			data.put("code", "1");
			data.put("message", "OK");
			builder = Response.status(Response.Status.OK).entity(data);
			
		}catch (Exception e) {
			e.printStackTrace();
			data.put("code", "99");
			data.put("message", e.getMessage());
			builder = Response.status(Response.Status.OK).entity(data);
			
		}	
		return builder.build();
	}

}
