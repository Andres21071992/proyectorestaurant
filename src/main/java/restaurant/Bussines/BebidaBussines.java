package restaurant.Bussines;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import restaurant.Dao.BebidaDao;
import restaurant.Models.Bebida;

@Stateless
public class BebidaBussines 
{
	@Inject
	private BebidaDao bebida;
	public boolean insertarBebida(Bebida b) throws Exception
	{
		Bebida aux= bebida.findBebida(b.getCodigo());
		boolean resp = true;
		if (aux == null)
		{
			try
			{
				bebida.insertBebida(b);
			}
			catch (Exception e)
			{
				resp = false;
			}
		}else
		{
			throw new Exception("bebida ya existe");
		}
		return resp;
	}
	public List<Bebida> obtenerBebida()
	{
		return bebida.getBebida();
	}
	public void eliminarBebida(String cod)
	{
		bebida.RemoveBebida(cod);
	}
	public void actualizarBebida(Bebida b)
	{
		bebida.updateBebida(b);
	}
}
