package restaurant.Bussines;

import java.util.List;

import javax.inject.Inject;

import restaurant.Dao.BebidaDao;
import restaurant.Dao.DetalleDao;
import restaurant.Models.Bebida;
import restaurant.Models.DetallePedido;

public class DetallePedidoBussines {
	
	
	@Inject
	private DetalleDao detalle;
	
	
	
	public boolean insertarDetalle(DetallePedido d) throws Exception
	{
		DetallePedido aux=  detalle.findDetalle(d.getCodigo()); 
		boolean resp = true;
		if (aux == null)
		{
			try
			{
				detalle.insertDetalle(d); 
			}
			catch (Exception e)
			{
				resp = false;
			}
		}else
		{
			throw new Exception("Detalle ya existe");
		}
		return resp;
	}
	public List<DetallePedido> obtenerDetallePedido()
	{
		return detalle.getDetallePedido();
	}
	public void eliminarDetallePedido(int cod)
	{
       detalle.RemovePedido(cod);
	}
	public void actualizarDetallePedido(DetallePedido d)
	{
	 detalle.updateDetalle(d);	
	 }
	

}
