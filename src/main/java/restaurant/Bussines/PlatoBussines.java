package restaurant.Bussines;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import restaurant.Dao.PlatoDao;
import restaurant.Models.Ingrediente;
import restaurant.Models.Platos;

@Stateless
public class PlatoBussines {

	@Inject
	private PlatoDao plato;
	
	public Platos getPlato(String cod)
	{
		return plato.findPlato(cod);
	}
	public List<Ingrediente> getIngrediente(String cod)
	{
		Platos p = plato.findPlato(cod);
		return p.getIngredientes();
	}
	
	public boolean insertarPlato(Platos p) throws Exception
	{
		Platos aux= plato.findPlato(p.getCodigo());
		boolean resp = true;
		if (aux == null)
		{
			try
			{
				plato.insertPlato(p);
			}
			catch (Exception e)
			{
				resp = false;
			}
		}else
		{
			throw new Exception("plato ya existe");
		}
		return resp;
	}
	public List<Platos> obtenerPlatos()
	{
		return plato.getPlatos();
	}
	public List<Platos> obtenerPlatosnombre(String nombre)
	{
		return plato.getPlatosxnombre(nombre);
	}
	public void eliminarPlato(String cod)
	{
		plato.RemovePlato(cod);
	}
	public void actualizarPlato(Platos p)
	{
		plato.updatePlato(p);
	}
	
	
}
