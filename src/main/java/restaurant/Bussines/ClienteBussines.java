package restaurant.Bussines;

import java.util.List;


import javax.ejb.Stateless;
import javax.inject.Inject;

import restaurant.Dao.ClienteDao;
import restaurant.Models.Cliente;
import restaurant.Models.Pedido;
import restaurant.Models.PedidoAux;
@Stateless
public class ClienteBussines {
	@Inject
	private ClienteDao cliente;	
	
	public List<Pedido> obtenerPedidos(String cedula)
	{
		Cliente aux = cliente.findCliente(cedula);
		return aux.getPedidos();
	}
	
	public boolean agregarPedido(PedidoAux pedido)
	{
		boolean result=true;
		Cliente aux = cliente.findCliente(pedido.getCedula());
		Pedido paux=new Pedido();
		paux.setFecha(pedido.getFpedido());
		paux.setTotal(pedido.getTotal());
		paux.setCodigo(pedido.getCodigo());
		aux.addPedido(paux);
		return result;
	}
	
	public Cliente buscarCliente(String cedula)
	{
		return cliente.findCliente(cedula);
	}
	
	
	
	public boolean insertarCliente(Cliente c) throws Exception
	{
		Cliente aux= cliente.findCliente(c.getCedula());
		boolean resp = true;
		if (aux == null)
		{
			try
			{
				cliente.insertCliente(c);
			}
			catch (Exception e)
			{
				resp = false;
			}
		}else
		{
			throw new Exception("persona existe");
		}
		return resp;
	}
	public List<Cliente> obtenerCliente()
	{
		return cliente.getCliente();
	}
	public void eliminarCliente(String ced)
	{
		cliente.RemoveCliente(ced);
	}
	public void actualizarCliente(Cliente c)
	{
		cliente.updateCliente(c);
	}
	public Cliente iniciarSesion(String usuario,String clave)
	{
		return cliente.logIn(usuario, clave);
	}

}
