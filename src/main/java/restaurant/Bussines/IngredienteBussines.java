package restaurant.Bussines;

import java.util.List;

import javax.inject.Inject;

import restaurant.Dao.IngredienteDao;
import restaurant.Models.Ingrediente;

public class IngredienteBussines {

	
	@Inject
	private IngredienteDao ingrediente;
	
	public boolean insertarIngrediente(Ingrediente i) throws Exception
	{
		Ingrediente aux= ingrediente.findIngrediente(i.getCod());
		boolean resp = true;
		if (aux == null)
		{
			try
			{
				ingrediente.insertIngrediente(i);
			}
			catch (Exception e)
			{
				resp = false;
			}
		}else
		{
			throw new Exception("Ingrediente Existe");
		}
		return resp;
	}
	public List<Ingrediente> obtenerIngrediente()
	{
		return ingrediente.getIngrediente(); 
	}
	public void eliminarIngrediente(int cod)
	{
		ingrediente.RemoveIngrediente(cod);
	}
	public void actualizarIngrediente(Ingrediente i)
	{
		ingrediente.updateIngrediente(i); 
	}	
}