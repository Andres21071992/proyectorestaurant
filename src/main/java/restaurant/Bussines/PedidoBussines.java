package restaurant.Bussines;

import java.util.List;
import javax.inject.Inject;
import restaurant.Dao.PedidoDao;
import restaurant.Dao.PlatoDao;
import restaurant.Models.DetallePedido;
import restaurant.Models.DetallePedidoAux;
import restaurant.Models.Pedido;
import restaurant.Models.Platos;

public class PedidoBussines {

	@Inject
	private PedidoDao pedido;
	@Inject
	private PlatoDao plato;
	
	
	public List<DetallePedido> obtenerDetalles(String codigo)
	{
		Pedido p=pedido.findPedido(codigo);
		return p.getDetalles();
	}
	public void calcular(String codigo)
	{
		Double total = 0.0;
		Pedido p=pedido.findPedido(codigo);
		List<DetallePedido> detalles = p.getDetalles();
		for (DetallePedido dp:detalles)
		{
			total = total + dp.getSubtotal();
		}
		p.setTotal(total);
		pedido.updatePedido(p);
	}
	public boolean insertarPedido(Pedido p) throws Exception
	{
		Pedido aux= pedido.findPedido(p.getCodigo());
		boolean resp = true;
		if (aux == null)
		{
			try
			{
				pedido.insertPedido(p);
			}
			catch (Exception e)
			{
				resp = false;
			}
		}else
		{
			throw new Exception("pedido duplicado");
		}
		return resp;
	}
	public List<Pedido> obtenerPedido()
	{
		return pedido.getPedido(); 
	}
	public void eliminarPedido(String cod)
	{
		pedido.RemovePedido(cod);
	}
	public void actualizarPedido(Pedido p)
	{
		pedido.updatePedido(p);
	}
	public void agregarDetallePedido(DetallePedidoAux detalle)
	{
		
		Platos p = plato.findPlato(detalle.getCodPlato());
		Pedido pe =pedido.findPedido(detalle.getCodPedido());
		DetallePedido detallepedido=new DetallePedido();
		detallepedido.setCantidad(detalle.getCantidad());
		detallepedido.setCodigo(detalle.getCodigo());
		detallepedido.setPunitario(detalle.getPunitario());
		detallepedido.setPlato( p);
		pe.addDetalle(detallepedido);
		
	}
	
}
