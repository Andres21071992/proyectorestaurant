package restaurant.Bussines;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import restaurant.Dao.UsuarioDao;
import restaurant.Models.Usuario;

@Stateless
public class UsuarioBussines {
	@Inject
	private UsuarioDao usuario;
	
	public Usuario login(String nombre,String clave)
	{
		return usuario.login(nombre, clave);
	}
	
	public void insertarUsuario(Usuario us)
	{
		usuario.insert(us);
	}
	
	public List<Usuario> obtenerUsuarios()
	{
		return usuario.getUsuarios();
	}
	
	public void eliminar(String cod)
	{
		usuario.remove(cod);
	}
	
	public void modificar(Usuario us)
	{
		usuario.modify(us);
	}
	
	
	
}
