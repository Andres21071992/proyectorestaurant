package restaurant.Bussines;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import restaurant.Dao.BebidaDao;
import restaurant.Dao.ClienteDao;
import restaurant.Dao.IngredienteDao;
import restaurant.Dao.PedidoDao;
import restaurant.Dao.PlatoDao;
import restaurant.Dao.StockDao;
import restaurant.Models.Bebida;
import restaurant.Models.Cliente;
import restaurant.Models.Ingrediente;
import restaurant.Models.Pedido;
import restaurant.Models.Platos;
import restaurant.Models.Stock;

@Stateless
public class ResBussines 
{
	@Inject
	private PlatoDao plato;
	
	@Inject
	private IngredienteDao ingrediente;
	
	@Inject
	private StockDao stk;
	
	@Inject
	private BebidaDao bebida;
	
	@Inject
	private ClienteDao cliente;
	
	@Inject
	private PedidoDao pedido;
	
	public boolean insertarStock(Stock stock)
	{
		boolean resp = true;
		try
		{
			stk.addStock(stock);
			
		}catch(Exception e)
		{
			e.printStackTrace();
			resp = false;
		}
		return resp;
	}
	
	public boolean actualizarStock(Stock stock)
	{
		boolean resp=true;
		try
		{
			stk.modifyStock(stock);
		}catch(Exception e)
		{
			e.printStackTrace();
			resp=false;
		}
		return resp;
	}
	
	public List<Stock> obtenerStock()
	{
		return stk.getStocks();
	}
	
	public boolean eliminarStock(String id)
	{
		boolean resp=true;
		stk.removeStock(id);
		return resp;
	}
	
	public boolean insertarIngrediente(Ingrediente i)
	{
		boolean resp=true;
		try
		{
		ingrediente.insertIngrediente(i);
		}
		catch(Exception e)
		{
			resp = false;
			e.printStackTrace();
		}
		return resp;
	}
	
	public List<Ingrediente> obtenerIngredientes()
	{
		return ingrediente.getIngrediente();
	}
	
	public boolean eliminarIngrediente(int cod)
	{
		boolean resp = true;
		try
		{
		ingrediente.RemoveIngrediente(cod);
		}
		catch(Exception e)
		{
			resp=false;
		}
		return resp;
	}
	public boolean actulizarIngrediente(Ingrediente i)
	{
		boolean resp=true;
		try
		{
			ingrediente.updateIngrediente(i);
		}
		catch(Exception e)
		{
			resp = false;
		}
		return resp;
	}
	public boolean insertarPlato(Platos p)
	{
		Platos aux= plato.findPlato(p.getCodigo());
		boolean resp = true;
		if (aux == null)
		{
			try
			{
				plato.insertPlato(p);
			}
			catch (Exception e)
			{
				resp = false;
			}
		}
		return resp;
	}
	public List<Platos> obtenerPlatos()
	{
		return plato.getPlatos();
	}
	public void eliminarPlato(String cod)
	{
		plato.RemovePlato(cod);
	}
	public void actualizarPlato(Platos p)
	{
		plato.updatePlato(p);
	}
	
	
			
	public boolean insertarCliente(Cliente c)
	{
		Cliente aux= cliente.findCliente(c.getCedula());
		boolean resp = true;
		if (aux == null)
		{
			try
			{
				cliente.insertCliente(c);
			}
			catch (Exception e)
			{
				resp = false;
			}
		}
		return resp;
	}
	public List<Cliente> obtenerCliente()
	{
		return cliente.getCliente();
	}
	public void eliminarCliente(String ced)
	{
		cliente.RemoveCliente(ced);
	}
	public void actualizarCliente(Cliente c)
	{
		cliente.updateCliente(c);
	}
	
	public boolean insertarBebida(Bebida b)
	{
		Bebida aux= bebida.findBebida(b.getCodigo());
		boolean resp = true;
		if (aux == null)
		{
			try
			{
				bebida.insertBebida(b);
			}
			catch (Exception e)
			{
				resp = false;
			}
		}
		return resp;
	}
	public List<Bebida> obtenerBebida()
	{
		return bebida.getBebida();
	}
	public void eliminarBebida(String cod)
	{
		bebida.RemoveBebida(cod);
	}
	public void actualizarBebida(Bebida b)
	{
		bebida.updateBebida(b);
	}
	
	public boolean insertarPedido(Pedido p)
	{
		Pedido aux= pedido.findPedido(p.getCodigo());
		boolean resp = true;
		if (aux == null)
		{
			try
			{
				pedido.insertPedido(p);
			}
			catch (Exception e)
			{
				resp = false;
			}
		}
		return resp;
	}
	public List<Pedido> obtenerPedidos()
	{
		return pedido.getPedido();
	}

	public void actualizarPedido(Pedido p)
	{
		pedido.updatePedido(p);
	}
		
}
