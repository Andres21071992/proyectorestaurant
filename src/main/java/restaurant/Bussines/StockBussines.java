package restaurant.Bussines;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import restaurant.Dao.StockDao;
import restaurant.Models.Stock;

@Stateless
public class StockBussines {
	@Inject
	private StockDao stk;
	
	public boolean insertarStock(Stock stock) throws Exception
	{
		boolean resp = true;
		Stock st=stk.findByidStock(stock.getCodigo());
		if (st == null)
		{	
			try
			{
				stk.addStock(stock);
				
			}catch(Exception e)
			{
				e.printStackTrace();
				resp = false;
			}
		}else
		{
			throw new Exception("producto ya existe");
		}
		return resp;
	}
	
	public boolean actualizarStock(Stock stock)
	{
		boolean resp=true;
		try
		{
			stk.modifyStock(stock);
		}catch(Exception e)
		{
			e.printStackTrace();
			resp=false;
		}
		return resp;
	}
	
	public List<Stock> obtenerStock()
	{
		return stk.getStocks();
	}
	
	public boolean eliminarStock(String id)
	{
		boolean resp=true;
		stk.removeStock(id);
		return resp;
	}
}
