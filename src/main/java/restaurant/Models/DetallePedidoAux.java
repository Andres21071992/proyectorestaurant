package restaurant.Models;

public class DetallePedidoAux {
	
	private int codigo;
	private int cantidad;
	private Double punitario;
	private Double subtotal;
	private String codPedido;
	private String codPlato;
	
	
	
	public String getCodPlato() {
		return codPlato;
	}
	public void setCodPlato(String codPlato) {
		this.codPlato = codPlato;
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public Double getPunitario() {
		return punitario;
	}
	public void setPunitario(Double punitario) {
		this.punitario = punitario;
	}
	public Double getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(Double subtotal) {
		this.subtotal = subtotal;
	}
	public String getCodPedido() {
		return codPedido;
	}
	public void setCodPedido(String codPedido) {
		this.codPedido = codPedido;
	}
	

	
}
