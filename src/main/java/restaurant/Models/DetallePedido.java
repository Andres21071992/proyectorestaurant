package restaurant.Models;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import restaurant.Vista.PlatoBean;

@Entity
public class DetallePedido {
	@Id
	@GeneratedValue
	private int codigo;
	private int cantidad;
	private Double punitario;
	private Double subtotal;
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="codigo_detalle")
	private Platos plato;
	
	public Platos getPlato() {
		return plato;
	}
	public void setPlato(Platos plato) {
		this.plato = plato;
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public Double getPunitario() {
		return punitario;
	}
	public void setPunitario(Double punitario) {
		this.punitario = punitario;
	}
	public Double getSubtotal() {
		return subtotal;
	}
	public void setSubtotal() {
		this.subtotal = this.cantidad * this.punitario;
	}
	
}
