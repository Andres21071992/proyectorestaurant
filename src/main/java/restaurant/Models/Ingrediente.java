package restaurant.Models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Ingrediente 
{
	@Id
	@GeneratedValue
	private int cod;
	@NotEmpty
	private String nombre;
	private float cantidad;
	private float porcion;
	@ManyToOne
	@JoinColumn(name="codigo")
	private Stock stock;
	
	public int getCod() {
		return cod;
	}
	public void setCod(int cod) {
		this.cod = cod;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public float getCantidad() {
		return cantidad;
	}
	public void setCantidad(float cantidad) {
		this.cantidad = cantidad;
	}
	public float getPorcion() {
		return porcion;
	}
	public void setPorcion(float porcion) {
		this.porcion = porcion;
	}
	
}
