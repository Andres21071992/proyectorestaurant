package restaurant.Models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Platos 
{
	@Id
	@NotEmpty
	private String codigo;
	@NotEmpty
	private String nombre;
	@NotNull
	private double precio;
	
	@OneToMany(cascade = {CascadeType.ALL},fetch=FetchType.EAGER)
	@JoinColumn(name ="plato")
	private List<Ingrediente> ingredientes;
	
	public List<Ingrediente> getIngredientes() {
		return ingredientes;
	}
	public void setIngredientes(List<Ingrediente> ingredientes) {
		this.ingredientes = ingredientes;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public void addIngrediente(Ingrediente in)
	{
		if(this.ingredientes == null)
			this.ingredientes = new ArrayList<>();
		this.ingredientes.add(in);
	}
	
}
