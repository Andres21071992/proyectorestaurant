package restaurant.Models;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;

import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.NotEmpty;



@Entity
public class Cliente {

	@Id
	@NotEmpty
	private String cedula;
	@NotEmpty
	private String nombre;
	@NotEmpty
	private String telefono;
	@NotEmpty
	private String direccion;
	@NotEmpty
	private String usuario;
	@NotEmpty
	private String clave;	
	
	@OneToMany(fetch=FetchType.EAGER,mappedBy="cliente",cascade = CascadeType.ALL)
	//@JsonIgnore
	private List<Pedido> pedidos;
	
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public List<Pedido> getPedidos() {
		return pedidos;
	}
	public void setPedidos(List<Pedido> pedidos) {
		this.pedidos = pedidos;
	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public void addPedido(Pedido p)
	{
		getPedidos().add(p);
		p.setCliente(this);
	}
	
}
 