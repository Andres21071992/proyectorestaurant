package restaurant.Dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import restaurant.Models.Bebida;
import restaurant.Models.Usuario;

@Stateless
public class UsuarioDao {
	@Inject
	private EntityManager em;
	public void insert(Usuario us)
	{
		em.persist(us);
	}
	public Usuario findus(String cedula)
	{
		Usuario usuario=em.find(Usuario.class, cedula);
		return usuario;
	}
	public void modify(Usuario us)
	{
		em.merge(us);
	}
	public void remove(String cod)
	{
		em.remove(findus(cod));
	}
	public List<Usuario> getUsuarios()
	{
		String jpql="SELECT u FROM Usuario u ";
		Query query =  em.createQuery(jpql,Usuario.class);
		List<Usuario> lista=  query.getResultList();
		return lista;
	}
	public Usuario login(String usuario,String clave)
	{
		String jpql="SELECT u FROM Usuario u WHERE u.usuario = :usuario AND u.clave = :clave";
		Query query =  em.createQuery(jpql,Usuario.class);
		query.setParameter("usuario", usuario);
		query.setParameter("clave", clave);
		Usuario us=(Usuario)  query.getSingleResult();
		return us;
	}
}
