package restaurant.Dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import restaurant.Models.Platos;

@Stateless
public class PlatoDao 	
{
	@Inject
	private EntityManager em;
	
	public void insertPlato(Platos p)
	{
		em.persist(p);
	}
	public void updatePlato(Platos p)
	{
		em.merge(p);
	}
	public Platos findPlato(String codigo)
	{
		Platos plato=em.find(Platos.class, codigo);
		return plato;
	}
	public void RemovePlato(String cod)
	{
		Platos plato=findPlato(cod);
		em.remove(plato);
	}
	public List<Platos> getPlatos()
	{
		String jpql="SELECT p FROM Platos p ";
		Query query =  em.createQuery(jpql,Platos.class);
		List<Platos> lista=  query.getResultList();
		return lista;
	}
	public List<Platos> getPlatosxnombre(String nombre)
	{
		String jpql="SELECT p FROM Platos p WHERE p.nombre like ?1";
		Query query =  em.createQuery(jpql,Platos.class);
		query.setParameter(1, "%"+nombre+"%");
		List<Platos> lista=  query.getResultList();
		return lista;
	}
}
