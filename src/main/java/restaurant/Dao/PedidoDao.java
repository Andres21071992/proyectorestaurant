package restaurant.Dao;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import restaurant.Models.Pedido;

@Stateless
public class PedidoDao {

	
	@Inject
	private EntityManager em;
	
	public void insertPedido(Pedido p)
	{
		em.persist(p);
	}
	public void updatePedido(Pedido p)
	{
		em.merge(p);
	}
	public Pedido findPedido(String codigo)
	{
		Pedido pedido=em.find(Pedido.class, codigo);
		return pedido;
	}
	public void RemovePedido(String cod)
	{
		em.remove(findPedido(cod));
	}
	public List<Pedido> getPedido()
	{
		String jpql="SELECT p FROM Pedido p ";
		Query query =  em.createQuery(jpql,Pedido.class);
		List<Pedido> lista=  query.getResultList();
		return lista;
	}

}
