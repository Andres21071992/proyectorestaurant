package restaurant.Dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import restaurant.Models.Stock;

@Stateless
public class StockDao 
{
	@Inject
	private EntityManager em;
	public void addStock(Stock stock)
	{
		em.persist(stock);
	}
	public void modifyStock(Stock stock)
	{
		em.merge(stock);
	}
	public Stock findByidStock(String Id)
	{
		Stock stock=em.find(Stock.class, Id);
		return stock;
	}
	public void removeStock(String id)
	{
		Stock stock=findByidStock(id);
		em.remove(stock);
	}
	public List<Stock> getStocks()
	{
		//List<Stock> lista = new ArrayList<>();
		String jpql="SELECT s FROM Stock s";
		Query query = em.createQuery(jpql,Stock.class);
		List<Stock> lista = query.getResultList();
		return lista;
	}
}
