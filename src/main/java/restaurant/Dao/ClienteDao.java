package restaurant.Dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import restaurant.Models.Cliente;

@Stateless
public class ClienteDao {
	@Inject
	private EntityManager em;
	
	public void insertCliente(Cliente c)
	{
		em.persist(c);
	}
	public void updateCliente(Cliente c)
	{
		em.merge(c);
	}
	public Cliente findCliente(String cedula)
	{
		Cliente cliente=em.find(Cliente.class, cedula);
		return cliente;
	}
	public void RemoveCliente(String ced)
	{
		em.remove(findCliente(ced));
	}
	public List<Cliente> getCliente()
	{
		String jpql="SELECT c FROM Cliente c ";
		Query query =  em.createQuery(jpql,Cliente.class);
		List<Cliente> lista=  query.getResultList();
		return lista;
	}
	public Cliente logIn(String usuario,String clave)
	{
		String jpql = "SELECT c FROM Cliente c WHERE c.usuario = :usu AND c.clave = :pass";
		Query query = em.createQuery(jpql,Cliente.class);
		query.setParameter("usu", usuario);
		query.setParameter("pass", clave);
		Cliente cliente =  (Cliente) query.getSingleResult();
		return cliente;
	}

}
