package restaurant.Dao;


import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import restaurant.Models.Bebida;

@Stateless
public class BebidaDao {
	
	@Inject
	private EntityManager em;
	
	public void insertBebida(Bebida b)
	{
		em.persist(b);
	}
	public void updateBebida(Bebida b)
	{
		em.merge(b);
	}
	public Bebida findBebida(String codigo)
	{
		Bebida bebida=em.find(Bebida.class, codigo);
		return bebida;
	}
	public void RemoveBebida(String cod)
	{
		em.remove(findBebida(cod));
	}
	public List<Bebida> getBebida()
	{
		String jpql="SELECT b FROM Bebida b ";
		Query query =  em.createQuery(jpql,Bebida.class);
		List<Bebida> lista=  query.getResultList();
		return lista;
	}
	

}
