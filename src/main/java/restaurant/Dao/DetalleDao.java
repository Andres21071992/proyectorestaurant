package restaurant.Dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import restaurant.Models.DetallePedido;;

@Stateless
public class DetalleDao {

	@Inject
	private EntityManager em;
	
	public void insertDetalle(DetallePedido d)
	{
		em.persist(d);
	}
	public void updateDetalle(DetallePedido d)
	{
		em.merge(d);
	}
	public DetallePedido findDetalle(int codigo)
	{
		DetallePedido detalle=em.find(DetallePedido.class, codigo);
		return detalle;
	}
	public void RemovePedido(int cod)
	{
		em.remove(findDetalle(cod));
	}
	public List<DetallePedido> getDetallePedido()
	{
		String jpql="SELECT d FROM DetallePedido d ";
		Query query =  em.createQuery(jpql,DetallePedido.class);
		List<DetallePedido> lista=  query.getResultList();
		return lista;
	}
}
