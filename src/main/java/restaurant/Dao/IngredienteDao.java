package restaurant.Dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import restaurant.Models.Ingrediente;

@Stateless
public class IngredienteDao 
{
	@Inject
	private EntityManager em;
	
	public void insertIngrediente(Ingrediente i)
	{
		em.persist(i);
	}
	public void updateIngrediente(Ingrediente i)
	{	
		em.merge(i);
	}
	public Ingrediente findIngrediente(int codigo)
	{
		Ingrediente ingrediente=em.find(Ingrediente.class, codigo);
		return ingrediente;
	}
	public void RemoveIngrediente(int cod)
	{
		em.remove(findIngrediente(cod));
	}
	public List<Ingrediente> getIngrediente()
	{
		String jpql="SELECT i FROM Ingrediente i ";
		Query query =  em.createQuery(jpql,Ingrediente.class);
		List<Ingrediente> lista=  query.getResultList();
		return lista;
	}
}
