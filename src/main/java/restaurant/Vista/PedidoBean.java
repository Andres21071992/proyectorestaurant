package restaurant.Vista;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import restaurant.Bussines.ResBussines;
import restaurant.Models.Ingrediente;
import restaurant.Models.Pedido;
import restaurant.Models.Platos;


@ManagedBean
@SessionScoped
public class PedidoBean {
	@Inject
	private ResBussines rb;

	
	private Pedido newPedido;
	private List<Pedido> pedidos;
	private boolean editar;
	
	@PostConstruct
	public void init()
	{
		newPedido = new Pedido();
		pedidos = rb.obtenerPedidos();	
		editar = false;
				
	}
	
	
	
	public boolean isEditar() {
		return editar;
	}

	public void setEditar(boolean editar) {
		this.editar = editar;
	}

	public Pedido getNewPedido() {
		return newPedido;
	}

	public void setNewPedido(Pedido newPedido) {
		this.newPedido = newPedido;
	}

	public List<Pedido> getPedidos() {
		return pedidos;
	}

	public void setPedidos(List<Pedido> pedidos) {
		this.pedidos = pedidos;
	}
	
	public String agregarPedido()
	{
		try
		{
			if (!editar)
			{
				rb.insertarPedido(newPedido);
				return "ListaPedidos?faces-redirect=true";
			}else
			{
				rb.actualizarPedido(newPedido);
				return "ListaPedidos?faces-redirect=true";
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			
		}
		return null;
	}
	public String editar(Pedido pedido)
	{
		editar=true;
		newPedido = pedido;
		return "Pedido.xhtml";
	}

	public String pgPrincipal()
	{
		editar = false;
		return "Pedido.xhtml";
	}

}
