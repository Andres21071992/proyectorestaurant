package restaurant.Vista;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import restaurant.Bussines.ClienteBussines;
import restaurant.Models.Cliente;
import restaurant.Models.Pedido;


@ManagedBean
@SessionScoped
public class ClienteBean {
	
	
	@Inject
	private ClienteBussines rb;
	@Inject
	private FacesContext facesContext;
	
	private Cliente newCliente;
	private List<Cliente> clientes;
	private boolean editar;

	
	@PostConstruct
	public void init()
	{
		newCliente = new Cliente();
		editar = false;
		clientes = rb.obtenerCliente();
	}
	
	public boolean isEditar() {
		return editar;
	}

	public void setEditar(boolean editar) {
		this.editar = editar;
	}

	public Cliente getNewCliente() {
		return newCliente;
	}

	public void setNewCliente(Cliente newCliente) {
		this.newCliente = newCliente;
	}

	public List<Cliente> getClientes() {
		return clientes;
	}

	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}
	
	public String agregarCliente()
	{
		try
		{
			if (!editar)
			{
				rb.insertarCliente(newCliente);
				init();
				return "ListaClientes?faces-redirect=true";
			}else
			{
				rb.actualizarCliente(newCliente);
				init();
				return "ListaClientes?faces-redirect=true";
			}
			
		}
		catch(Exception e)
		{
			 FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), "Error ");
	            facesContext.addMessage(null, m);
		}
		return null;
	}
	public String editar(Cliente cliente)
	{
		editar=true;
		newCliente = cliente;
		return "Cliente.xhtml";
	}
	public String eliminarCliente(String ced)
	{
		rb.eliminarCliente(ced);
		init();
		return "ListaClientes?faces-redirect=true";
	}
	public String pgPrincipal()
	{
		editar = false;
		return "Cliente.xhtml";
	}
	

}