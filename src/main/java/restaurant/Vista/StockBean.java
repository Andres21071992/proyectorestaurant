package restaurant.Vista;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import restaurant.Bussines.ResBussines;
import restaurant.Bussines.StockBussines;
import restaurant.Models.Ingrediente;
import restaurant.Models.Stock;

@ManagedBean
public class StockBean {
	@Inject 
	private StockBussines rb;
	@Inject
	private FacesContext facesContext;
	
	public Stock newStock;
	public List<Stock> stocks;
	public boolean editar;
	
	@PostConstruct
	public void init()
	{
		newStock = new Stock();
		stocks = rb.obtenerStock();
		editar = false;
	}

	public Stock getNewStock() {
		return newStock;
	}

	public void setNewStock(Stock newStock) {
		this.newStock = newStock;
	}

	public List<Stock> getStocks() {
		return stocks;
	}

	public void setStocks(List<Stock> stocks) {
		this.stocks = stocks;
	}

	public boolean isEditar() {
		return editar;
	}

	public void setEditar(boolean editar) {
		this.editar = editar;
	}
	
	public String agregarIngrediente()
	{
		try
		{
			if (!editar)
			{
				boolean estado=rb.insertarStock(newStock);
				return "ListaStock?faces-redirect=true";
			}else
			{
				rb.actualizarStock(newStock);
				return "ListaStock?faces-redirect=true";
			}
			
		}
		catch(Exception e)
		{
			 FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), "Error ");
	            facesContext.addMessage(null, m);
		}
		return null;
	}
	public String editar(Stock stock)
	{
		editar=true;
		newStock = stock;
		return "Stock.xhtml";
	}
	public String eliminarStock(String cod)
	{
		rb.eliminarStock(cod);
		return "ListaStock?faces-redirect=true";
	}
	public String pgPrincipal()
	{
		editar = false;
		return "Stock.xhtml";
	}
}
