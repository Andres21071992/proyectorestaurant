package restaurant.Vista;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import restaurant.Bussines.BebidaBussines;

import restaurant.Models.Bebida;
import restaurant.Models.Stock;


@ManagedBean
public class BebidaBean {
	
	@Inject 
	private BebidaBussines rb;
	@Inject
	private FacesContext facesContext;
	public Bebida newBebida;
	public List<Bebida> bebidas;
	public boolean editar=false;
	

	
	@PostConstruct
	public void init()
	{
		newBebida = new Bebida();
		bebidas = rb.obtenerBebida();
		editar = false;
	}

	
	public Bebida getNewBebida() {
		return newBebida;
	}

	public void setNewBebida(Bebida newBebida) {
		this.newBebida = newBebida;
	}



	public List<Bebida> getBebidas() {
		return bebidas;
	}

	public void setBebidas(List<Bebida> bebidas) {
		this.bebidas = bebidas;
	}

	public boolean isEditar() {
		return editar;
	}

	public void setEditar(boolean editar) {
		this.editar = editar;
	}
	
	
	/*
	 <h:selectOneRadio value="#{bebidaBean.editar}">
	<f:selectItem itemValue="true" itemLabel="EDITAR"/>
	</h:selectOneRadio>
	 */
	public String guardar()
	{
		try
		{
		if(editar==true) {
			rb.actualizarBebida(newBebida);
			editar=false;
			return "ListaBebidas?faces-redirect=true";
		}else {
			rb.insertarBebida(newBebida);
			return "ListaBebidas?faces-redirect=true";
		}
		}catch(Exception e)
		{
			 FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), "Error ");
	            facesContext.addMessage(null, m);
		}
		return null;
		
	}
	
	public String editar(Bebida bebida)
	{
		editar=true;
		newBebida = bebida;
		return "Bebida.xhtml";
	}
	public String eliminarBebida(String cod)
	{
		rb.eliminarBebida(cod);
		return "ListaBebidas?faces-redirect=true";
	}
	public String pgPrincipal()
	{
		editar = false;
		return "Bebida.xhtml";
	}
	

}
