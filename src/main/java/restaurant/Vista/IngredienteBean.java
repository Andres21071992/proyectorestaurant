package restaurant.Vista;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import restaurant.Bussines.ResBussines;
import restaurant.Models.Ingrediente;


@ManagedBean
public class IngredienteBean 
{
	@Inject
	private ResBussines rb;
	
	
	private Ingrediente newIngrediente;
	private List<Ingrediente> ingredientes;
	private boolean editar;
	
	@PostConstruct
	public void init()
	{
		newIngrediente = new Ingrediente();
		ingredientes = rb.obtenerIngredientes();	
		editar = false;
	}
	
	public boolean isEditar() {
		return editar;
	}

	public void setEditar(boolean editar) {
		this.editar = editar;
	}

	public Ingrediente getNewIngrediente() {
		return newIngrediente;
	}

	public void setNewIngrediente(Ingrediente newIngrediente) {
		this.newIngrediente = newIngrediente;
	}

	public List<Ingrediente> getIngredientes() {
		return ingredientes;
	}

	public void setIngredientes(List<Ingrediente> ingredientes) {
		this.ingredientes = ingredientes;
	}
	
	public String agregarIngrediente()
	{
		try
		{
			if (!editar)
			{
				rb.insertarIngrediente(newIngrediente);
				return "ListaIngredientes?faces-redirect=true";
			}else
			{
				rb.actulizarIngrediente(newIngrediente);
				return "ListaIngredientes?faces-redirect=true";
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	public String editar(Ingrediente ingrediente)
	{
		editar=true;
		newIngrediente = ingrediente;
		return "Ingrediente.xhtml";
	}
	public String eliminarIngrediente(int cod)
	{
		rb.eliminarIngrediente(cod);
		return null;
	}
	public String pgPrincipal()
	{
		editar = false;
		return "Ingrediente.xhtml";
	}
}
