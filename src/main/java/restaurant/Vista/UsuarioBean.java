package restaurant.Vista;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import restaurant.Bussines.UsuarioBussines;
import restaurant.Models.Usuario;

@ManagedBean
public class UsuarioBean {

	
	@Inject
	private UsuarioBussines usBussines;
	
	@Inject
	private FacesContext facesContext;
	
	
	private Usuario newUsuario;
	
	private boolean editing;
	
	private String usuario;
	
	private String clave;
	
	@PostConstruct
	public void init() {
		newUsuario = new Usuario ();
		
		editing = false;
	}

	
	
	
	
	
	
	
	
	public UsuarioBussines getUsBussines() {
		return usBussines;
	}









	public void setUsBussines(UsuarioBussines usBussines) {
		this.usBussines = usBussines;
	}









	public Usuario getNewUsuario() {
		return newUsuario;
	}









	public void setNewUsuario(Usuario newUsuario) {
		this.newUsuario = newUsuario;
	}









	public boolean isEditing() {
		return editing;
	}









	public void setEditing(boolean editing) {
		this.editing = editing;
	}









	public String getUsuario() {
		return usuario;
	}









	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}









	public String getClave() {
		return clave;
	}









	public void setClave(String clave) {
		this.clave = clave;
	}









	public String login() {
		
		try {
			if (usuario != null && clave != null) {
				System.out.println("verificando login");
				
				newUsuario = usBussines.login(usuario, clave);
				
				if (newUsuario != null) {
					return "index2?faces-redirect=true";
				}else {
					return "index?faces-redirect=true";
				}			
			}
			
		} catch (Exception e) {
			System.out.println("No se puede iniciar sesion");
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), "Error");
			facesContext.addMessage(null, m);
		}
		return "";
	}
}
