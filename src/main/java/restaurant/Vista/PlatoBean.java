package restaurant.Vista;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import restaurant.Bussines.PlatoBussines;
import restaurant.Bussines.ResBussines;
import restaurant.Models.Ingrediente;
import restaurant.Models.Platos;

@ManagedBean
@SessionScoped
public class PlatoBean 
{
	@Inject
	private PlatoBussines rb;
	@Inject
	private FacesContext facesContext;
	private Platos newPlato;
	private List<Platos> platos;
	private boolean edit;
	
	
	@PostConstruct
	public void init()
	{
		newPlato = new Platos();
		newPlato.addIngrediente(new Ingrediente());
		edit = false;
		platos = rb.obtenerPlatos();
	}
	
	public String addIngrediente()
	{
		newPlato.addIngrediente(new Ingrediente());
		return null;
	}
	
	public boolean isEdit() {
		return edit;
	}

	public void setEdit(boolean edit) {
		this.edit = edit;
	}

	public Platos getNewPlato() {
		return newPlato;
	}

	public void setNewPlato(Platos newPlato) {
		this.newPlato = newPlato;
	}

	public List<Platos> getPlatos() {
		return platos;
	}

	public void setPlatos(List<Platos> platos) {
		this.platos = platos;
	}

	public String agregarPlato()
	{
		try
		{
			if (!edit)
			{
				rb.insertarPlato(newPlato);
				init();
				return "ListaPlatos?faces-redirect=true";
			}else
			{
				rb.actualizarPlato(newPlato);
				init();
				return "ListaPlatos?faces-redirect=true";
			}
			
		}
		catch(Exception e)
		{
			 FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), "Error ");
	            facesContext.addMessage(null, m);
			
		}
		return null;
	}
	
	public String eliminarPlato(String codigo)
	{
		rb.eliminarPlato(codigo);
		init();
		return "ListaPlatos?faces-redirect=true";
	}
	
	public String editar(Platos plato)
	{
		edit=true;
		newPlato = plato;
		return "Plato.xhtml";
	}
	public String Pgprincipal()
	{
		edit = false;
		return "Plato.xhtml";
	}
}	

	
